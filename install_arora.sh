dnf config-manager --set-enabled crb
dnf install epel-release epel-next-releas

chmod +x fedora_script.sh
./fedora_script.sh

# install needed packages (open pdf, load page)
dnf -y install arora
dnf -y install xorg-x11-server-Xorg xinit
dnf -y install mupdf
dnf -y install twm

echo "xterm & exec twm" > .xinitrc

reboot