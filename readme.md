# Project BPC-SOS
This is a project to subject BPC-SOS. 
Operating system is the latest version of [CentOS](https://www.centos.org/).

```
localhost login: root
Password: student
```


Total size of the operating system:
```
du / --exclude=/{proc,sys,dev} -abc | sort -n
```

# Showing page and pdf file
To start an emulator of the terminal: 
```
startx
```

Command for uploading pdf file to CentOS: 
```
scp OS_theory_classic.pdf root@<IP_address_of_host_adapter>:/root
```

Command for showing pdf file: 
```
mupdf OS_theory_classic.pdf
```

Command for showing pdf file: 
```
arora www.vutbr.cz
```


# Reduce a size of the CentOS
1. script _minimization.sh_ contains:
- Deleting temporary log files and cache files,
- removing localizations and all manual pages, information
and program documentation,
- removing unused kernel modules,
- removing firmware.

2. script _minimization_pcg.sh_ contains:
- Deleting unnecessary applications (python),
- removing unused packages.